#include "day-of-year.hpp"
#include <array>

int dayOfYear(int month, int dayOfMonth, int year) {
    std::array<int, 12> months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    if (year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)) {
        months[1] = 29;
    }
    
    for (int i = 1; i < month; ++i) {
        dayOfMonth += months[i - 1];
    }

    return dayOfMonth;
}

