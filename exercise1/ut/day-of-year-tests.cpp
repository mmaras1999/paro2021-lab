#include "gtest/gtest.h"
#include "day-of-year.hpp"

TEST(DayOfYearTestSuite1, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite2, December31stIsLastDayOfLeap)
{
  ASSERT_EQ(dayOfYear(12, 31, 2020), 366);
  ASSERT_EQ(dayOfYear(12, 31, 400), 366);
}

TEST(DayOfYearTestSuite3, December31stIsLastDayOfNoLeapYear)
{
  ASSERT_EQ(dayOfYear(12, 31, 2021), 365);
  ASSERT_EQ(dayOfYear(12, 31, 100), 365);
}

TEST(DayOfYearTestSuite4, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite5, RandomDate)
{
  ASSERT_EQ(dayOfYear(5, 9, 1990), 129);
  ASSERT_EQ(dayOfYear(10, 31, 2003), 304);
  ASSERT_EQ(dayOfYear(6, 23, 2003), 174);
}
