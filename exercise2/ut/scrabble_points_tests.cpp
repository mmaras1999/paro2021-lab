#include "gtest/gtest.h"
#include "scrabble.hpp"

TEST(ScrabbleTestSuite1, SingleCharacter)
{
  ASSERT_EQ(scrabble_points("a"), 1);
  ASSERT_EQ(scrabble_points("b"), 3);
  ASSERT_EQ(scrabble_points("z"), 10);
  ASSERT_EQ(scrabble_points("A"), 1);
  ASSERT_EQ(scrabble_points("B"), 3);
  ASSERT_EQ(scrabble_points("Z"), 10);
}

TEST(ScrabbleTestSuite2, LongerWords)
{
  ASSERT_EQ(scrabble_points("PARKING"), 14);
  ASSERT_EQ(scrabble_points("TRAIN"), 5);
  ASSERT_EQ(scrabble_points("ZOMbIFiCaTION"), 31);
}
