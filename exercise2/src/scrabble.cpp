#include "scrabble.hpp"
#include <string>
#include <array>
#include <map>
#include <cassert>

int scrabble_points(std::string word) {
    std::map<char, int> character_points = {
        {'a', 1},
        {'e', 1}, 
        {'i', 1}, 
        {'o', 1}, 
        {'u', 1}, 
        {'l', 1}, 
        {'n', 1}, 
        {'r', 1}, 
        {'s', 1},
        {'t', 1},
        {'d', 2}, 
        {'g', 2},
        {'b', 3},
        {'c', 3},
        {'m', 3},
        {'p', 3},
        {'f', 4}, 
        {'h', 4}, 
        {'v', 4}, 
        {'w', 4},
        {'y', 4},
        {'k', 5},
        {'j', 8},
        {'x', 8},
        {'q', 10},
        {'z', 10}
    };

    int res = 0;

    for(auto c : word) {
        c = std::tolower(c);
        assert(character_points.count(c) != 0);
        res += character_points[c];
    }         

    return res;
}

